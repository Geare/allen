const express = require('express');
const bodyparser = require('body-parser')
const  app =express();
const http = require('http');
const fs = require('fs');
var datetime = new Date().toLocaleString();
let fibreq;
app.use(bodyparser.urlencoded({ extended: true}));
app.use(bodyparser.text());
let serv2;
let server =app.listen(8003, function () {
    fs.appendFile('log.txt',("BOOT "+datetime+"\n") , function (err) {
        if (err) throw err;
        console.log('boot');
    });//when the server starts we mark it to our log file
    //we also mark it to console so we know that it started ok
    console.log("server is running!");
});

app.get("/", (req, res) => {
    console.log("message received");
    let requestOptions = {
        hostname: 'service2',
        port: 8004,
        path: '/',
        method: 'GET'
    };

    http.get(requestOptions,(res)=>{
        res.on('data', d => {
            process.stdout.write(d);
            console.log(d);
            serv2 = d;
        })
    });

        res.send('Hello from' + req.connection.remoteAddress + req.client.remotePort + 'to' + req.connection.localAddress + req.client.localPort+ serv2);


});
app.post("/fibo", (req, fibreq)=>{
    
    var i;
var fib = []; // Initialize array
fib[0] = 0;
fib[1] = 1;
for (i = 2; i <= 10; i++) {
  fib[i] = fib[i - 2] + fib[i - 1];
  //delay the calculation in case we want to stop it
}
fibreq.send(fib.toString())

console.log(fibreq.body);
});
app.get("/run-log" ,function () {
    
    fs.readFile('log.txt','utf8', (err, data) => {
        if (err) throw err;
        console.log(data);
      });
       

});
app.post("/shutdown",(req,res)=>{
    
    let requestOptions = {
        hostname: 'service2',
        port: 8004,
        path: '/',
        method: 'POST'
    };
    fs.appendFile('log.txt',("SHUTDOWN "+datetime+"\n") , function (err) {
        if (err) throw err;
        console.log('shutdown')});
    http.get(requestOptions,(res)=>{
        res.on(server.close());
    
    });
   
    
    
});


