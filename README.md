READ ME 

The follwoing ports are exposed by the app: 8003 and 8004

The docker-compose file is located in allen/dockerexercise1

To start the container navigate to that file and run the command docker-compose up --build

to access the hello function from the previous exercise use the path GET “/”  on port 8003, I highly recommend making the requests in this project using Postman or a tool similar to it.

To calulate the Fibonacci sequence use POST "/fib", this will return the first 10 numbers in the sequence with the first two being hardcoded

To see the boot/shutdown log use the path GET /run-log, this will return an indicator in the console of what action happened and at what time. It is being read from a file that is written to each time that either event happens. When you make the request it needs to be manually terminated after the log has appeared in the terminal

To shutdown the system use the path POST "Localhost:8003/shutdown". First the second service will shutdown after sending a notification and then the first service will shutdown, this command also kills the docker containers,


